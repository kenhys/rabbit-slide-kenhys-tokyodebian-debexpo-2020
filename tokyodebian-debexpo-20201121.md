# debexpo(mentors.d.n)をハックするには

subtitle
:  (2020年版)

author
:  Kentaro Hayashi

institution
:  ClearCode Inc.

content-source
:  2020年11月 東京エリア・関西合同Debian勉強会

allotted-time
:  15m

theme
:  .

# お知らせ: スライドは公開済みです

* この資料はRabbit Slide Showで閲覧できます
  * debexpo(mentors.d.n)をハックするには(2020年版)
    * <https://slide.rabbit-shocker.org/authors/kenhys/tokyodebian-debexpo-20201121/>
    

# プロフィール

![](images/profile.png){:relative-height="40"}

* ひよこDebian Developer (@kenhys) 2020/09-
* トラックポイント(ソフトドーム派)

# 本日の内容

* mentors.debian.netについて
* 前に発表したときから変わったこと
* 開発環境のつくりかた
* まとめ

# mentors.debian.netとは

* コントリビューターとスポンサーをつなぐサイト
  * 一時的なアップロード先として使える
  * RFSの雛形も用意してくれる
  * パッケージのチェック結果をWebで確認できる

# 前回からのあらすじ(2016-06-25)

* debexpoをハックするには
  * 東京Debian勉強会 140回
  * mentors.d.nの残念なところをどうにかしようとした話
     * <https://slide.rabbit-shocker.org/authors/kenhys/tokyodebian-debexpo-20160625/>

# その後の主な動き

* GitHubからSalsaへ移行した
* Python2からPython3に移行した
* PylonsからDjangoへ移行した
* メッセージの国際化対応(フランス語)

# 開発環境を構築するには

* 必要なものをインストール
* ホストの設定を追加する
* サーバーを起動する
* ワーカーを起動する

# 必要なものをインストール

```sh
$ git clone https://salsa.debian.org/mentors.debian.net-team/debexpo.git
$ cd debexpo
$ python3 setup.py build
$ python3 setup.py install
$ python3 setup.py develop
$ python3 manage.py migrate
$ python3 manage.py collectstatic --no-input --clear
```

* パッケージでいれるかvirtualenv使うかはお好みで

# ホストの設定を追加する

```sh
diff --git a/debexpo/settings/develop.py b/debexpo/settings/develop.py
index 7b7121b7..3e8c2cbc 100644
--- a/debexpo/settings/develop.py
+++ b/debexpo/settings/develop.py
@@ -35,7 +35,7 @@ SECRET_KEY = 'DoNotUseThisKeyInProductionEnvironment'
 # SECURITY WARNING: don't run with debug turned on in production!
 DEBUG = True
 
-ALLOWED_HOSTS = []
+ALLOWED_HOSTS = ["*"]
```
* これをしておかないとdputできない

# サーバーを起動する


```sh
$ python3 manage.py runserver 0.0.0.0:8000
```

* 他のホストからアクセスしないなら0.0.0.0:8000は不要
* 依存関係を嫌ってVMを立てたときは忘れずに

# ワーカーを起動する

```sh
$ celery --app debexpo worker --beat
```

* バックグラウンドジョブを走らせる
  * 例：パッケージをインポートする

# ワーカーが正常に起動できると


```sh
$ celery --app debexpo inspect registered
-> celery@buster: OK
    * debexpo.accounts.tasks.CleanupAccounts
    * debexpo.importer.tasks.importer
    * debexpo.packages.tasks.remove_old_uploads
    * debexpo.packages.tasks.remove_uploaded_packages
    * debexpo.repository.tasks.remove_from_repository
```

# アップロードする前に

```sh
[debexpo]
fqdn = localhost:8000
incoming = /upload
method = http
allow_unsigned_uploads = 0
```

* `~/.dput.cf` に設定を追加する

# アカウントを作成する

* サインインのリンクを踏む
* フォームへアカウントの情報を入力する
* mboxを参照してアクティベート
* GPGの鍵を登録する

# サインインのリンクを踏む

![](images/signin-debexpo.png)

* 画面右上のSign me upをクリックする

# フォームへアカウントの情報を入力する

![](images/signup-form-debexpo.png)

* 名前とメールアドレスを入力する

# mboxを参照してアクティベート

```sh
$ find data/mbox/
data/mbox/
data/mbox/20201107-082449-140269976552592.log
```

* メールの内容がmbox以下に作成される

# アカウントのアクティベート

```sh
Content-Type: text/plain; charset="utf-8"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit
Subject: Next step: Confirm your email address
From: debexpo <support@example.org>
To: kenhys@gmail.com
Date: Sat, 07 Nov 2020 08:24:49 -0000
Message-ID: <160473748915.1139.18222488963671024124@localhost>

Hello,

Please activate your account by visiting the following address
in your web-browser:

http://192.168.121.172:8000/accounts/reset/MQ/5le-acd00fdca7302c8dee26/

If you didn't create an account on debexpo,
you can safely ignore this email.

Thanks,
```

# GPGの鍵を登録する

```sh
gpg --export --export-options export-minimal --armor keyid
```

* フォームに上記の結果を貼り付けて登録する

# パッケージをアップロードする

```sh
$ dput debexpo xxx.changes
```

* パッケージに署名するのを忘れずに

# 明示的にインポートするには

```sh
$ celery --app debexpo call debexpo.importer.tasks.importer
```

* すぐにインポートしたいときに使う

# 最近やったこと

* すでにDebianにはいっているかどうか区別できるようにする
  * <https://salsa.debian.org/mentors.debian.net-team/debexpo/-/merge_requests/167>

# 修正画面イメージ

![](images/packages-list.png){:relative-width="90"}

# Packages listの関連ファイル

```sh
% find debexpo/packages/
debexpo/packages/
debexpo/packages/serializers.py
debexpo/packages/views.py
debexpo/packages/__init__.py
debexpo/packages/migrations
debexpo/packages/migrations/0001_initial.py
debexpo/packages/migrations/__init__.py
debexpo/packages/apps.py
debexpo/packages/tasks.py
debexpo/packages/templates
debexpo/packages/templates/package.html
debexpo/packages/templates/packages.html
debexpo/packages/templates/packages-list.html
debexpo/packages/templates/email-upload-removed.html
debexpo/packages/models.py
```

# Packages listの関連スキーマ

```sh
 sqlite3 db.sqlite3 
SQLite version 3.27.2 2019-02-25 16:06:06
Enter ".help" for usage hints.
sqlite> .schema packages_package
CREATE TABLE IF NOT EXISTS "packages_package" (
"id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
"name" varchar(100) NOT NULL UNIQUE,
"needs_sponsor" bool NOT NULL,
"in_debian" bool NOT NULL);
```

# Packages listのviewを修正

```sh
diff --git a/debexpo/packages/templates/packages-list.html b/debexpo/packages/templates/packages-list.html
index 263f8e50..299b84f0 100644
--- a/debexpo/packages/templates/packages-list.html
+++ b/debexpo/packages/templates/packages-list.html
@@ -7,10 +7,11 @@
 <table width="100%">
     <tr>
         <th width="15%">{% trans 'Package' %}</th>
-        <th width="40%">{% trans 'Description' %}</th>
+        <th width="30%">{% trans 'Description' %}</th>
         <th width="20%">{% trans 'Version' %}</th>
         <th width="15%">{% trans 'Uploader' %}</th>
         <th width="10%">{% trans 'Needs a sponsor?' %}</th>
+        <th width="10%">{% trans 'Already in Debian' %}</th>
     </tr>
 
     {% for pkg in group.packages %}
@@ -32,6 +33,13 @@
             {% trans 'No' %}
         {% endif %}
         </td>
+        <td class="lines">
+        {% if pkg.in_debian %}
+            {% trans 'Yes' %}
+        {% else %}
+            {% trans 'No' %}
+        {% endif %}
+        </td>
     </tr>
     {% endfor %}
 </table>
```

# まとめ

* mentors.d.nは継続的に開発が続いているよ
* DjangoアプリなのでPythonな人はいじりやすいかも
* 解決したいissueがいっぱいあるよ
  * <https://salsa.debian.org/mentors.debian.net-team/debexpo/-/issues>
* 気になるところがあればぜひフィードバックしよう
